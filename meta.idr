infixr 5 ->>
infixr 5 <->
||| Logical Implication
data (->>) : a -> b -> Type where

||| Logical Biconditional
data (<->) : a -> b -> Type where

||| Logical negation
data n : x -> Type where

syntax "~_" [a] = (n a);

||| Propositions. Types can be interpreted like them because we need some examples
data Prop : (x : a) -> Type where 
  Imp : Prop x -> Prop y -> Prop (x ->> y)
  Bicond : Prop x -> Prop y -> Prop (x <-> y)
  Not : Prop x -> Prop (n x)
  Example : (x : Type) -> Prop x

||| Something is provable. It obviously has to be a Proposition, but this is stated in the constructors.
||| The constructors correspondend with standard axiom schemes.

--infixl 7 |-
data Prf : {b : Type} -> (a : b) -> Type where
  ax_1  : Prop x -> Prop y -> Prf (x ->> (y ->> x))
  ax_2  : Prop x -> Prop y -> Prop z -> Prf ((x ->> (y ->> z)) ->> ((x ->> y) ->> (x ->> z)))
  ax_3  : Prop x -> Prop y -> Prf ((x ->> y) ->> (~_y ->> ~_x))
  ax_mp : Prop x -> Prop y -> Prf (x ->> y) -> Prf x -> Prf y 

syntax "|- " [a] "-> " [b] = (Prf a) -> b;
syntax "|-" [a] "." = (Prf a);
syntax "3P ->" [a] = Prop x -> Prop y -> Prop z -> a;
syntax "2P ->" [a] = Prop x -> Prop y -> a;
syntax "P ->" [a] = Prop x -> a;
syntax "4P ->" [a] = Prop x -> Prop y -> Prop z -> Prop p -> a;
||| Double use of modus ponens.
mp2 : Prop x -> Prop y -> Prop z -> |- x -> |- y -> |- (x ->> (y ->> z)) -> |- z .
mp2 x y z x1 y1 impl = ax_mp y z (ax_mp x (Imp y z) impl x1) y1

||| True stays true, even after implication (ax1 as rule)
a1i : 2P -> |- x -> |- (y ->> x) .
a1i px py x = ax_mp px (Imp py px) (ax_1 px py) x

||| Nested use of modus ponens.
mp2b : 3P -> |- x -> |- (x ->> y) -> |- (y ->> z) -> |- z . 
mp2b px py pz x xy yz = ax_mp py pz yz (ax_mp px py xy x)

||| Drop and reconnect of antecedent
mp1i : 3P -> |- x -> |- (x ->> y) -> |- (z ->> y) .
mp1i px py pz x xy = a1i py pz (ax_mp px py xy x)


||| ax2 as rule
a2i : 3P -> |- (x ->> (y ->> z)) -> |- ((x ->> y) ->> (x ->> z)) .
a2i px py pz xyz = ax_mp (Imp px (Imp py pz)) (Imp (Imp px py) (Imp px pz)) (ax_2 px py pz) xyz

||| Adding common antecendents doesn't change truth.
imim2i : 3P -> |- (x ->> y) -> |- ((z ->> x) ->> (z ->> y)) .
imim2i px py pz xy = a2i pz px py (a1i (Imp px py) pz xy)

||| Modus ponens deduction
mpd : 3P -> |- (x ->> y) -> |- (x ->> (y ->> z)) -> |- (x ->> z) .
mpd px py pz xy xyz = ax_mp (Imp px py) (Imp px pz) (a2i px py pz xyz) xy

||| Implication is transitive
syl : 3P -> |- (x ->> y) -> |- (y ->> z) -> |- (x ->> z) .
syl px py pz xy yz = mpd px py pz xy (a1i (Imp py pz) px yz)

||| Another nested modus ponens inference.
mpi : 3P -> |- y -> |- (x ->> (y ->> z)) -> |- (x ->> z) .
mpi px py pz y xyz = mpd px py pz (a1i py px y) xyz

||| Chaining two syllogisms
syl3 : 4P -> |- (x ->> y) -> |- (y ->> z) -> |- (z ->> p) -> |- (x ->> p) .

syl3 px py pz pp xy yz zp = syl px pz pp (syl px py pz xy yz) zp

||| Everything implies itself
idt : P -> |- (x ->> x) .
idt px = mpd px (Imp px px) px (ax_1 px px) (ax_1 px (Imp px px))

||| Identity with antecedent
idd : 2P -> |- (x ->> (y ->> y)) .
idd px py = a1i (Imp py py) px (idt py)

||| Deduction with an embedded antecedent
a1d : 3P -> |- (x ->> y) -> |- (x ->> (z ->> y)) .
a1d px py pz xy = syl px py (Imp pz py) xy (ax_1 py pz)

||| Deduction distributing an embedded antecedent
a2d : 4P -> |- (x ->> y ->> z ->> p) -> |- (x ->> (y ->> z) ->> (y ->> p)) . 
a2d px py pz pp xyzp = syl px (Imp py (Imp pz pp)) (Imp (Imp py pz) (Imp py pp)) xyzp (ax_2 py pz pp)

||| Add two antecedents
a1ii : 3P -> |- z -> |- (x ->> y ->> z) .
a1ii px py pz z = a1i (Imp py pz) px (a1i pz py z)
